﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace bank
{
	class Program
	{
		static void Main(string[] args)
		{

			// I create 2 clases, currentAccount and savingsBank, that these inherit from bankAccount.
			//the diference is when extract money, you must pay to current Account a discount but you don't pay to savings Bank. For example.
			bankAccount member1 = new currentAccount("Juan", "12345", "pesos", 35000);
			Console.WriteLine("I create a current account with following data:");
			member1.Show();
			Console.WriteLine("------------------------------------------");
			bankAccount member2 = new savingsBank("Juan", "12345", "pesos", 35000);
			Console.WriteLine("Now, I create a savings bank with following data:");
			member2.Show();
			Console.WriteLine("------------------------------------------");
			Console.WriteLine("Let's extract $3000 from current acount:");
			member1.ExtractMoney(3000);
			Console.WriteLine("balance is discount because, despite extracting $3000, the current account charge you 20%. you finish discount to you account $3600");
			Console.WriteLine("------------------------------------------");
			Console.WriteLine("Now,extract $3000 from the savings bank:");
			member2.ExtractMoney(3000);
			Console.WriteLine("instead of it, savings bank do not charge you to extract money");//... savings bank is of neighborhood(?).
			//we see the concept of polimorphism, how a object can be created by polimorphism and how it can be call.
			Console.ReadKey();

		}
	}
}

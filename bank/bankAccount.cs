﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace bank
{
	public abstract class bankAccount
	{
		string owner;
		string code;
		string typeMoney;
		double balance;

		protected bankAccount(string owner, string code, string typeMoney, double balance)
		{
			this.owner = owner;
			this.code = code;
			this.typeMoney = typeMoney;
			this.balance = balance;
		}

		public string Owner { get => owner; set => owner = value; }
		public string Code { get => code; set => code = value; }
		public string TypeMoney { get => typeMoney; set => typeMoney = value; }
		public double Balance { get => balance; set => balance = value; }

		public virtual void ExtractMoney(double mount)
		{
			Balance = Balance - mount;
		}
		public abstract void Show();
	}
}

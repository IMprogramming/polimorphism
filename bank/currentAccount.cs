﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace bank
{
	public class currentAccount : bankAccount
	{
		double discount = 20;
		public currentAccount(string owner, string code, string typeMoney, double balance) : base(owner, code, typeMoney, balance)
		{

		}

		public double Discount { get => discount; set => discount = value; }

		public override void ExtractMoney(double mount)
		{
			//must pay a comission for using service.
			Balance = Balance - (mount * (1 + (Discount/100)));
			Console.WriteLine("The sum of $ {0} was given to {1}.",mount,Owner);
			Console.WriteLine("Current Balance: ${0}",Balance);
		}
		public override void Show()
		{
			//this method shows data of the objects. Can we improve this?
			Console.WriteLine($"Owner: {Owner}");
			Console.WriteLine($"Code: {Code}");
			Console.WriteLine($"Money: {TypeMoney}");
			Console.WriteLine($"Balance: {Balance}");
			Console.WriteLine($"Discount: {Discount}%");
		}
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace bank
{
	class savingsBank : bankAccount
	{
		public savingsBank(string owner, string code, string typeMoney, double balance) : base(owner, code, typeMoney, balance)
		{

		}

		public override void ExtractMoney(double mount)
		{
			//supose that we don't pay for using this account, then will not pay a add.
			Balance = Balance - mount;
			Console.WriteLine("The sum of $ {0} was given to {1}.", mount, Owner);
			Console.WriteLine("Current Balance: ${0}", Balance);
		}
		public override void Show()
		{
			//this method shows data of the objects. Can we improve this?
			Console.WriteLine($"Owner: {Owner}");
			Console.WriteLine($"Code: {Code}");
			Console.WriteLine($"Money: {TypeMoney}");
			Console.WriteLine($"Balance: {Balance}");
		}
	}
}
